# simple-composer
Ce projet montre la création d'un package PHP de base intégré à `Packagist` avec la mise en place de l'auto-update via GitLab.

https://packagist.org/packages/polytech/simple-composer

## Installation du package
`composer require polytech/simple-composer`

## Utilisation du package
```
<?php
require_once __DIR__ . '/vendor/autoload.php';
use Polytech\Polytech;
echo Polytech::greet("Hello Angers\n");
```
